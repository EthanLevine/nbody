#include <stdlib.h>
#include <string.h>

#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_math.h>

#include "alg.h"
#include "state.h"

struct brute_state {
    struct state inner;
};

static void *brute_import(struct state state, int verbosity) {
    verbosity;
    struct brute_state *bs = malloc(sizeof(*bs));
    bs->inner.vec = malloc(state.count * sizeof(struct point));
    bs->inner.count = state.count;
    memcpy(bs->inner.vec, state.vec, state.count * sizeof(struct point));
    return bs;
}

static struct state brute_export(void *state, int verbosity) {
    verbosity;
    return ((struct brute_state *)state)->inner;
}

static void brute_step(void *state, float step_time, int verbosity) {
    verbosity;
    struct brute_state *bs = (struct brute_state *)state;
    // first update all positions
    float *pos_x_new = malloc(bs->inner.count * sizeof(float));
    float *pos_y_new = malloc(bs->inner.count * sizeof(float));
    for (int i = 0; i < bs->inner.count; i++) {
        pos_x_new[i] = bs->inner.vec[i].x + step_time * bs->inner.vec[i].u;
        pos_y_new[i] = bs->inner.vec[i].y + step_time * bs->inner.vec[i].v;
    }
    // then update all velocities using all-pairs method.
    for (int i = 0; i < bs->inner.count; i++) {
        float accel_x = 0;
        float accel_y = 0;
        for (int j = 0; j < bs->inner.count; j++) {
            if (i == j) {
                continue;
            }
            float R = pdist(bs->inner.vec[i], bs->inner.vec[j]);
            float force_comp = GSL_CONST_MKSA_GRAVITATIONAL_CONSTANT * bs->inner.vec[j].m / (R*R*R);
            accel_x += (bs->inner.vec[j].x - bs->inner.vec[i].x) * force_comp;
            accel_y += (bs->inner.vec[j].y - bs->inner.vec[i].y) * force_comp;
        }
        bs->inner.vec[i].u += step_time * accel_x;
        bs->inner.vec[i].v += step_time * accel_y;
    }
    // copy new positions back
    for (int i = 0; i < bs->inner.count; i++) {
        bs->inner.vec[i].x = pos_x_new[i];
        bs->inner.vec[i].y = pos_y_new[i];
    }
    free(pos_x_new);
    free(pos_y_new);
}

struct algorithm alg_brute = {
    "brute",
    brute_import,
    brute_export,
    brute_step
};

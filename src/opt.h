#pragma once

#include "gen.h"

struct arguments {
    char *algorithm;
    char *input_file;
    char *output_file;
    long steps;
    int verbosity;
    float step_time;
    struct gen_spec gen_spec;
};

void parse_all(int argc, char **argv, struct arguments *arguments);
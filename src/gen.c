#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "state.h"
#include "gen.h"

static void rand_init() {
    srand(time(NULL));
}

static float rand_float() {
    float ret = (float)rand()/RAND_MAX;
    ret = (rand() + ret)/RAND_MAX;
    ret = (rand() + ret)/RAND_MAX;
    ret = (rand() + ret)/RAND_MAX;
    ret = (rand() + ret)/RAND_MAX;
    ret = (rand() + ret)/RAND_MAX;
    return ret;
}

static void gen_plain(struct gen_spec *spec, struct state *state, int verbosity) {
    verbosity;
    state->count = spec->count;
    state->vec = malloc(spec->count * sizeof(struct point));
    for (int i = 0; i < spec->count; i++) {
        state->vec[i].m = rand_float() * (spec->mass_size / 2) + (spec->mass_size / 2);
        state->vec[i].x = rand_float() * spec->pos_size * 2 - spec->pos_size;
        state->vec[i].y = rand_float() * spec->pos_size * 2 - spec->pos_size;
        state->vec[i].u = rand_float() * spec->vel_size * 2 - spec->vel_size;
        state->vec[i].v = rand_float() * spec->vel_size * 2 - spec->vel_size;
    }
}

// Returns 1 if there is gen spec, 0 if there isn't, and exits on error.
int gen_check_spec(struct gen_spec *spec) {
    // Determine if a spec is defined.
    if (spec->type == NULL) {
        // There ISN'T a spec
        if (spec->count != 0 ||
                spec->pos_size != 0 ||
                spec->vel_size != 0 ||
                spec->mass_size != 0) {
            return -1;
        }
        return 0;
    } else {
        // There IS a spec
        if (spec->count < 2) {
            return -1;
        }
        if (spec->pos_size == 0) {
            return -1;
        }
        if (spec->vel_size == 0) {
            return -1;
        }
        if (spec->mass_size == 0) {
            return -1;
        }
        return 1;
    }
}

void gen_state(struct gen_spec *spec, struct state *state, int verbosity) {
    rand_init();

    if (strcmp(spec->type, "plain") == 0) {
        gen_plain(spec, state, verbosity);
    } else {
        errx(3, "Generator \"%s\" not recognized.", spec->type);
    }
}

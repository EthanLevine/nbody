#include <err.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "alg.h"
#include "state.h"

#include <CL/opencl.h>

#include "alg_ptree_kerns.clpp"

#define CHECK_RESULT(result) do { if ((result) != CL_SUCCESS) errx(6, "oh no: %i @ %i", (result), __LINE__); } while (0)

struct bounds {
    float xmin;
    float xmax;
    float ymin;
    float ymax;
};

struct quadnode {
    long pp;
    long pz;
    long zp;
    long zz;
    union {
        struct bounds bounds;
        struct {
            float x;
            float y;
        } fixpoint;
    } pos;
};

struct quadtree {
    struct quadnode *nodes;
    struct {
        cl_float2 *pos;
        cl_float *mass;
    } cells;
    struct {
        cl_float2 *pos;
        cl_float2 *vel;
        cl_float *mass;
    } points;
    struct {
        cl_float2 *pos;
        cl_float *mass;
        struct bounds *bounds;
        size_t *count;
    } leaves;

    size_t node_count;
    size_t point_count;
    size_t block_size;

    size_t max_leaves;
    size_t max_cells;

    long root;
};

struct ptree_state {
    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program_pp;
    cl_program program_pl;
    cl_program program_pc;
    cl_program program_update;
    cl_kernel kern_pp; // point-to-point
    cl_kernel kern_pl; // point-to-leaf
    cl_kernel kern_pc; // point-to-cell
    cl_kernel kern_update; // updates w/ accelerations

    struct quadtree tree;

    cl_mem buff_cell_pos;
    cl_mem buff_cell_mass;
    cl_mem buff_point_pos;
    cl_mem buff_point_accel;
    cl_mem buff_point_vel;
    cl_mem buff_point_mass;
    cl_mem buff_leaf_pos;
    cl_mem buff_leaf_mass;
};

static float select_median_5(float *data, size_t count) {
    float old_max = INFINITY;
    float new_max;
    for (unsigned int j = 0; j < (count/2 + 1); j++) {
        new_max = -INFINITY;
        for (unsigned int i = 0; i < count; i++) {
            if (data[i] < old_max && data[i] > new_max)
                new_max = data[i];
        }
        old_max = new_max;
    }
    if (count/2 == 0)
        return (new_max + old_max) / 2;
    else
        return new_max;
}

static float select_median(float *data, size_t count) {
    if (count <= 5) {
        return select_median_5(data, count);
    } else {
        for (unsigned int i = 0; i < count/5; i++) {
            data[i] = select_median_5(data + 5*i, 5);
        }
        data[count/5] = select_median_5(data + 5*(count/5), count % 5);
        return select_median(data, (count+4)/5);
    }
}

static void insert_point_quadtree(struct quadtree *qt, long *node_ref,
            cl_float2 pt_pos, cl_float2 pt_vel, cl_float pt_mass) {
    long node = *node_ref;
    if (node >= 0) {
        struct quadnode *qn = qt->nodes + node;
        long *next_node;
        if (pt_pos.x > qn->pos.fixpoint.x) {
            if (pt_pos.y > qn->pos.fixpoint.y)
                next_node = &qn->pp;
            else
                next_node = &qn->pz;
        } else {
            if (pt_pos.y > qn->pos.fixpoint.y)
                next_node = &qn->zp;
            else
                next_node = &qn->zz;
        }
        if (*next_node == 0) {
            // setup a new leaf for this.
            // find the first empty leaf
            long i;
            for (i = 0; qt->leaves.count[i] != 0; i++);
            *next_node = -i-1;
        }
        insert_point_quadtree(qt, next_node, pt_pos, pt_vel, pt_mass);
    } else {
        long leaf_id = -node-1;
        if (qt->leaves.count[leaf_id] == qt->block_size) {
            // split leaf into node
            // find median in X and Y directions
            float *buff = malloc(qt->block_size * sizeof(float));
            for (unsigned int i = 0; i < qt->block_size; i++) {
                buff[i] = qt->points.pos[leaf_id*qt->block_size + i].x;
            }
            float x_median = select_median(buff, qt->block_size);
            for (unsigned int i = 0; i < qt->block_size; i++) {
                buff[i] = qt->points.pos[leaf_id*qt->block_size + i].y;
            }
            float y_median = select_median(buff, qt->block_size);
            free(buff);
            // setup a new cell for this.
            qt->nodes[qt->node_count].pp = 0;
            qt->nodes[qt->node_count].pz = 0;
            qt->nodes[qt->node_count].zp = 0;
            qt->nodes[qt->node_count].zz = 0;
            qt->nodes[qt->node_count].pos.fixpoint.x = x_median;
            qt->nodes[qt->node_count].pos.fixpoint.y = y_median;
            *node_ref = qt->node_count;
            qt->node_count++;
            // recycle old leaf
            qt->leaves.count[leaf_id] = 0;
            /*******************************
             * UNSAFE OPTIMIZATION HERE !! *
             *******************************
             The points in leaf leaf_id are technically NOT part of the tree.
             That leaf CAN BE REWRITTEN WHILE WE ARE RE-ADDING THE POINTS.
             This is OKAY because we can never OVERWRITE what we are reading,
             but others should be aware of this tricky optimization...
             */
            // add all points in leaf to new leaves
            for (unsigned int i = 0; i < qt->block_size; i++) {
                unsigned int id = leaf_id*qt->block_size + i;
                insert_point_quadtree(qt, node_ref, qt->points.pos[id], qt->points.vel[id], qt->points.mass[id]);
            }
            // add current point to new leaves, finally.
            insert_point_quadtree(qt, node_ref, pt_pos, pt_vel, pt_mass);
        } else {
            // just add node to leaf
            int id = leaf_id*qt->block_size + qt->leaves.count[leaf_id];
            qt->points.pos[id] = pt_pos;
            qt->points.vel[id] = pt_vel;
            qt->points.mass[id] = pt_mass;
            qt->leaves.count[leaf_id]++;
        }
    }
}

static void calculate_cells(struct quadtree *qt, long cell_id) {
    if (cell_id < 0) {
        // this is a leaf
        int leaf_id = -cell_id-1;
        // initialize bounds
        struct bounds *bounds = qt->leaves.bounds + leaf_id;
        bounds->xmin = INFINITY;
        bounds->xmax = -INFINITY;
        bounds->ymin = INFINITY;
        bounds->ymax = -INFINITY;
        // initialize center of mass
        float cx = 0;
        float cy = 0;
        float cm = 0;
        // loop through leaf
        for (unsigned int i = 0; i < qt->leaves.count[leaf_id]; i++) {
            unsigned int id = leaf_id*qt->block_size + i;
            // push bounds
            cl_float2 pos = qt->points.pos[id];
            cl_float mass = qt->points.mass[id];
            if (bounds->xmin > pos.x)
                bounds->xmin = pos.x;
            if (bounds->xmax < pos.x)
                bounds->xmax = pos.x;
            if (bounds->ymin > pos.y)
                bounds->ymin = pos.y;
            if (bounds->ymax < pos.y)
                bounds->ymax = pos.y;
            // add to center of mass
            cx = cx * cm + pos.x * mass;
            cy = cy * cm + pos.y * mass;
            cm += mass;
            cx /= cm;
            cy /= cm;
        }
        // set center of mass
        qt->leaves.pos[leaf_id].x = cx;
        qt->leaves.pos[leaf_id].y = cy;
        qt->leaves.mass[leaf_id] = cm;
    } else {
        struct quadnode *node = &qt->nodes[cell_id];
        // calculate children
        if (node->pp != 0) calculate_cells(qt, node->pp);
        if (node->pz != 0) calculate_cells(qt, node->pz);
        if (node->zp != 0) calculate_cells(qt, node->zp);
        if (node->zz != 0) calculate_cells(qt, node->zz);
        // measure contribution
        // this is a bit messy due to the tree structure
        cl_float2 child_pos[4];
        cl_float child_mass[4];
        long children[4] = {node->pp, node->pz, node->zp, node->zz};
        node->pos.bounds.xmin = INFINITY;
        node->pos.bounds.xmax = -INFINITY;
        node->pos.bounds.ymin = INFINITY;
        node->pos.bounds.ymax = -INFINITY;
        for (int i = 0; i < 4; i++) {
            if (children[i] == 0) {
                child_mass[i] = 0;
                child_pos[i].x = 0;
                child_pos[i].y = 0;
            } else {
                struct bounds *bounds;
                if (children[i] > 0) {
                    bounds = &qt->nodes[children[i]].pos.bounds;
                    child_pos[i] = qt->cells.pos[children[i]];
                    child_mass[i] = qt->cells.mass[children[i]];
                } else {
                    long leaf_id = -children[i]-1;
                    bounds = &qt->leaves.bounds[leaf_id];
                    child_pos[i] = qt->leaves.pos[leaf_id];
                    child_mass[i] = qt->leaves.mass[leaf_id];
                }
                if (bounds->xmin < node->pos.bounds.xmin)
                    node->pos.bounds.xmin = bounds->xmin;
                if (bounds->xmax > node->pos.bounds.xmax)
                    node->pos.bounds.xmax = bounds->xmax;
                if (bounds->ymin < node->pos.bounds.ymin)
                    node->pos.bounds.ymin = bounds->ymin;
                if (bounds->ymax > node->pos.bounds.ymax)
                    node->pos.bounds.ymax = bounds->ymax;
            }
        }
        cl_float center_mass = child_mass[0] + child_mass[1] + child_mass[2] + child_mass[3];
        cl_float center_x = (child_pos[0].x*child_mass[0] +
                             child_pos[1].x*child_mass[1] +
                             child_pos[2].x*child_mass[2] +
                             child_pos[3].x*child_mass[3]) / center_mass;
        cl_float center_y = (child_pos[0].y*child_mass[0] +
                             child_pos[1].y*child_mass[1] +
                             child_pos[2].y*child_mass[2] +
                             child_pos[3].y*child_mass[3]) / center_mass;
        qt->cells.pos[cell_id].x = center_x;
        qt->cells.pos[cell_id].y = center_y;
        qt->cells.mass[cell_id] = center_mass;
    }
}

static void initialize_quadtree(struct quadtree *qt, size_t point_count, struct point *points) {
    // max #leaves = ((#points + BS - 1) / BS) * 6 - 2
    // max #cells = ((#points + BS - 1) / BS) * 2 - 1
    qt->point_count = point_count;
    size_t worst_blocks = (qt->point_count + qt->block_size - 1) / qt->block_size;
    qt->max_leaves = worst_blocks * 6 - 3; // technically, one extra leaf
                                           // for buffering purposes on split
    qt->max_cells = worst_blocks * 2 - 1;

    qt->nodes = malloc(qt->max_cells * sizeof(struct quadnode));
    qt->cells.pos = malloc(qt->max_cells * sizeof(cl_float2));
    qt->cells.mass = malloc(qt->max_cells * sizeof(cl_float));
    qt->points.pos = malloc(qt->max_leaves * qt->block_size * sizeof(cl_float2));
    qt->points.vel = malloc(qt->max_leaves * qt->block_size * sizeof(cl_float2));
    qt->points.mass = malloc(qt->max_leaves * qt->block_size * sizeof(cl_float));
    qt->leaves.pos = malloc(qt->max_leaves * sizeof(cl_float2));
    qt->leaves.mass = malloc(qt->max_leaves * sizeof(cl_float));
    qt->leaves.bounds = malloc(qt->max_leaves * sizeof(struct bounds));
    qt->leaves.count = malloc(qt->max_leaves * sizeof(size_t));

    // step 2: prepare empty tree
    qt->node_count = 0;
    qt->root = -1;
    memset(qt->leaves.count, 0, qt->max_leaves * sizeof(size_t));
    memset(qt->points.mass, 0, qt->max_leaves * qt->block_size * sizeof(cl_float));

    // step 3: fill with all points
    for (unsigned int i = 0; i < qt->point_count; i++) {
        // note: double braces are needed because cl_float2's first member is
        //       an array of size 2, like:  struct cl_float2 {s[2]};
        cl_float2 pos = {{ points[i].x, points[i].y }};
        cl_float2 vel = {{ points[i].u, points[i].v }};
        cl_float mass = points[i].m;
        insert_point_quadtree(qt, &qt->root, pos, vel, mass);
    }

    // step 4: calculate mass centers and bounding boxes
    calculate_cells(qt, qt->root);
}

static void set_quadtree(struct quadtree *qt, cl_float2 *point_pos, cl_float2 *point_vel,
        cl_float *point_mass, size_t chunk_max_size, size_t *chunk_sizes, size_t chunk_count) {
    qt->node_count = 0;
    qt->root = -1;
    memset(qt->leaves.count, 0, qt->max_leaves * sizeof(size_t));
    memset(qt->points.mass, 0, qt->max_leaves * qt->block_size * sizeof(cl_float));

    for (unsigned int i = 0; i < chunk_count; i++) {
        for (unsigned int j = 0; j < chunk_sizes[i]; j++) {
            unsigned int id = i*chunk_max_size + j;
            insert_point_quadtree(qt, &qt->root, point_pos[id], point_vel[id], point_mass[id]);
        }
    }

    calculate_cells(qt, qt->root);
}

static void *ptree_import(struct state state, int verbosity) {
    struct ptree_state *pts = malloc(sizeof(struct ptree_state));

    // Fill out the OpenCL parts of ptree_state.
    if (verbosity >= 2) fprintf(stderr, "Probing OpenCL hardware...\n");
    cl_uint num_platforms;
    cl_int result = clGetPlatformIDs(0, NULL, &num_platforms);
    CHECK_RESULT(result);
    if (verbosity >= 2) fprintf(stderr, "Found %i platforms.\n", num_platforms);
    if (num_platforms == 0) errx(6, "No OpenCL platforms found!");
    cl_platform_id *platform_ids = malloc(num_platforms * sizeof(cl_platform_id));
    result = clGetPlatformIDs(num_platforms, platform_ids, NULL);
    CHECK_RESULT(result);
    // if (verbosity >= 2) fprintf(stderr, "Using platform ID %i.\n", platform_ids[0]);
    pts->platform = platform_ids[0];

    cl_uint num_devices;
    result = clGetDeviceIDs(pts->platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);
    CHECK_RESULT(result);
    if (verbosity >= 2) fprintf(stderr, "Found %i devices.\n", num_devices);
    if (num_devices == 0) errx(6, "No devices found!");
    cl_device_id *device_ids = malloc(num_devices * sizeof(cl_device_id));
    result = clGetDeviceIDs(pts->platform, CL_DEVICE_TYPE_GPU, num_devices, device_ids, NULL);
    CHECK_RESULT(result);
    // if (verbosity >= 2) fprintf(stderr, "Using device ID %i.\n", device_ids[0]);
    pts->device = device_ids[0];

    result = clGetDeviceInfo(pts->device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(pts->tree.block_size), &pts->tree.block_size, NULL);
    CHECK_RESULT(result);
    if (verbosity >= 2) fprintf(stderr, "Device has max WG size of %zu.\n", pts->tree.block_size);
    
    if (verbosity >= 2) fprintf(stderr, "Initializing quadtree...\n");
    initialize_quadtree(&pts->tree, state.count, state.vec);

    pts->context = clCreateContext(NULL, 1, &pts->device, NULL, NULL, &result);
    CHECK_RESULT(result);
    pts->queue = clCreateCommandQueue(pts->context, pts->device, 0, &result);
    CHECK_RESULT(result);
    if (verbosity >= 2) fprintf(stderr, "Compiling and creating kernels...\n");
    pts->program_pp = clCreateProgramWithSource(pts->context, 1, &ptree_kern_pp, NULL, &result);
    CHECK_RESULT(result);
    pts->program_pc = clCreateProgramWithSource(pts->context, 1, &ptree_kern_pc, NULL, &result);
    CHECK_RESULT(result);
    pts->program_pl = clCreateProgramWithSource(pts->context, 1, &ptree_kern_pl, NULL, &result);
    CHECK_RESULT(result);
    pts->program_update = clCreateProgramWithSource(pts->context, 1, &ptree_kern_update, NULL, &result);
    CHECK_RESULT(result);

    result = clBuildProgram(pts->program_pp, 1, device_ids, "", NULL, NULL);
    CHECK_RESULT(result);
    result = clBuildProgram(pts->program_pc, 1, device_ids, "", NULL, NULL);
    CHECK_RESULT(result);
    result = clBuildProgram(pts->program_pl, 1, device_ids, "", NULL, NULL);
    CHECK_RESULT(result);
    result = clBuildProgram(pts->program_update, 1, device_ids, "", NULL, NULL);
    CHECK_RESULT(result);

    pts->kern_pp = clCreateKernel(pts->program_pp, "ptree_kern_pp", &result);
    CHECK_RESULT(result);
    pts->kern_pc = clCreateKernel(pts->program_pc, "ptree_kern_pc", &result);
    CHECK_RESULT(result);
    pts->kern_pl = clCreateKernel(pts->program_pl, "ptree_kern_pl", &result);
    CHECK_RESULT(result);
    pts->kern_update = clCreateKernel(pts->program_update, "ptree_kern_update", &result);
    CHECK_RESULT(result);

    // Initialize buffers (they will be written at every stage)
    if (verbosity >= 2) fprintf(stderr, "Creating all buffers...\n");
    pts->buff_cell_pos = clCreateBuffer(pts->context, CL_MEM_READ_ONLY, pts->tree.max_cells * sizeof(cl_float2), NULL, &result);
    CHECK_RESULT(result);
    pts->buff_cell_mass = clCreateBuffer(pts->context, CL_MEM_READ_ONLY, pts->tree.max_cells * sizeof(cl_float), NULL, &result);
    CHECK_RESULT(result);
    
    pts->buff_point_pos = clCreateBuffer(pts->context, CL_MEM_READ_WRITE, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), NULL, &result);
    CHECK_RESULT(result);
    pts->buff_point_accel = clCreateBuffer(pts->context, CL_MEM_READ_WRITE, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), NULL, &result);
    CHECK_RESULT(result);
    pts->buff_point_vel = clCreateBuffer(pts->context, CL_MEM_READ_WRITE, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), NULL, &result);
    CHECK_RESULT(result);
    pts->buff_point_mass = clCreateBuffer(pts->context, CL_MEM_READ_ONLY, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float), NULL, &result);
    CHECK_RESULT(result);

    pts->buff_leaf_pos = clCreateBuffer(pts->context, CL_MEM_READ_ONLY, pts->tree.max_leaves * sizeof(cl_float2), NULL, &result);
    CHECK_RESULT(result);
    pts->buff_leaf_mass = clCreateBuffer(pts->context, CL_MEM_READ_ONLY, pts->tree.max_leaves * sizeof(cl_float), NULL, &result);
    CHECK_RESULT(result);

    // Set most kernel parameters
    if (verbosity >= 2) fprintf(stderr, "Setting kernel arguments...\n");
    result = clSetKernelArg(pts->kern_pp, 0, sizeof(cl_mem), &pts->buff_point_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pp, 1, sizeof(cl_mem), &pts->buff_point_mass);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pp, 2, sizeof(cl_mem), &pts->buff_point_accel);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pp, 3, pts->tree.block_size * sizeof(cl_float2), NULL);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pp, 4, pts->tree.block_size * sizeof(cl_float), NULL);
    CHECK_RESULT(result);

    result = clSetKernelArg(pts->kern_pc, 0, sizeof(cl_mem), &pts->buff_point_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pc, 1, sizeof(cl_mem), &pts->buff_point_accel);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pc, 2, sizeof(cl_mem), &pts->buff_cell_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pc, 3, sizeof(cl_mem), &pts->buff_cell_mass);
    CHECK_RESULT(result);

    result = clSetKernelArg(pts->kern_pl, 0, sizeof(cl_mem), &pts->buff_point_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pl, 1, sizeof(cl_mem), &pts->buff_point_accel);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pl, 2, sizeof(cl_mem), &pts->buff_leaf_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_pl, 3, sizeof(cl_mem), &pts->buff_leaf_mass);
    CHECK_RESULT(result);

    result = clSetKernelArg(pts->kern_update, 0, sizeof(cl_mem), &pts->buff_point_pos);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_update, 1, sizeof(cl_mem), &pts->buff_point_vel);
    CHECK_RESULT(result);
    result = clSetKernelArg(pts->kern_update, 2, sizeof(cl_mem), &pts->buff_point_accel);
    CHECK_RESULT(result);

    free(platform_ids);
    free(device_ids);
    return pts;
}

static struct state ptree_export(void *state, int verbosity) {
    struct ptree_state *pts = (struct ptree_state *)state;
    struct state estate;
    estate.count = pts->tree.point_count;
    estate.vec = malloc(estate.count * sizeof (struct point));

    // step 3: read data back to newly allocated host buffers
    cl_float2 *new_pos_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2));
    cl_float2 *new_vel_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2));
    cl_float *new_mass_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float));
    size_t *chunk_sizes_buff = malloc(pts->tree.max_leaves * sizeof(size_t));

    // read positions, velocities, and masses
    if (verbosity >= 2) fprintf(stderr, "Queueing buffer reads from GPU...\n");
    cl_int result = clEnqueueReadBuffer(pts->queue, pts->buff_point_pos, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), new_pos_buff, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueReadBuffer(pts->queue, pts->buff_point_vel, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), new_vel_buff, 0, NULL, NULL);
    CHECK_RESULT(result);
    memcpy(new_mass_buff, pts->tree.points.mass, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float));
    memcpy(chunk_sizes_buff, pts->tree.leaves.count, pts->tree.max_leaves * sizeof(size_t));

    result = clFinish(pts->queue);
    CHECK_RESULT(result);

    // Copy into the export state
    unsigned int pt_id = 0;
    for (unsigned int i = 0; i < pts->tree.max_leaves; i++) {
        for (unsigned int j = 0; j < chunk_sizes_buff[i]; j++) {
            unsigned int id = i*pts->tree.block_size + j;
            struct point *pt = estate.vec + (pt_id++);
            pt->m = new_mass_buff[id];
            pt->x = new_pos_buff[id].x;
            pt->y = new_pos_buff[id].y;
            pt->u = new_vel_buff[id].x;
            pt->v = new_vel_buff[id].y;
        }
    }

    free(new_pos_buff);
    free(new_vel_buff);
    free(new_mass_buff);
    free(chunk_sizes_buff);

    return estate;
}

static float calc_dist(float x1, float y1, float x2, float y2) {
    float dx = x1 - x2;
    float dy = y1 - y2;
    return sqrtf(dx*dx + dy*dy);
}

static float calc_dist_bounds(struct bounds *b1, struct bounds *b2) {
    if (b1->xmin > b2->xmax) {
        if (b1->ymin > b2->ymax) {
            // bottom left - compare (xmin1, ymin1) (xmax2, ymax2)
            return calc_dist(b1->xmin, b1->ymin, b2->xmax, b2->ymax);
        } else if (b1->ymax < b2->ymin) {
            // top left - compare (xmin1, ymax1) (xmax2, ymin2)
            return calc_dist(b1->xmin, b1->ymax, b2->xmax, b2->ymin);
        } else {
            // middle left - compare xmin1 xmax2;
            return b1->xmin - b2->xmax;
        }
    } else if (b1->xmax < b2->xmin) {
        if (b1->ymin > b2->ymax) {
            // bottom right - compare (xmax1, ymin1) (xmin2, ymax2)
            return calc_dist(b1->xmax, b1->ymin, b2->xmin, b2->ymax);
        } else if (b1->ymax < b2->ymin) {
            // top right - compare (xmax1, ymax1) (xmin2, ymin2)
            return calc_dist(b1->xmax, b1->ymax, b2->xmin, b2->ymin);
        } else {
            // middle right - compare xmax1 xmin2
            return b2->xmin - b1->xmax;
        }
    } else {
        if (b1->ymin > b2->ymax) {
            // bottom center - compare ymin1 ymax2
            return b1->ymin - b2->ymax;
        } else if (b1->ymax < b2->ymin) {
            // top center - compare ymax1 ymin2
            return b2->ymin - b2->ymax;
        } else {
            // middle center (inside) - return 0
            return 0;
        }
    }
}

static float calc_size(struct bounds bounds) {
    float dx = bounds.xmax - bounds.xmin;
    float dy = bounds.ymax - bounds.ymin;
    return dx > dy ? dx : dy;
}

static void ptree_step_node(struct ptree_state *pts, long src_leaf_id, long node_id,
        float threshold, int verbosity) {
    struct quadtree *qt = &pts->tree;
    cl_int result;
    cl_int arg;
    if (node_id >= 0) {
        // we're comparing to a cell
        // calculate distance
        float dist = calc_dist_bounds(&qt->leaves.bounds[src_leaf_id], &qt->nodes[node_id].pos.bounds);
        float size = calc_size(qt->nodes[node_id].pos.bounds);
        if (dist / size > threshold) {
            // calculate as single cell
            arg = (cl_int)(src_leaf_id * qt->block_size);
            result = clSetKernelArg(pts->kern_pc, 4, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            arg = (cl_int)(node_id);
            result = clSetKernelArg(pts->kern_pc, 5, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            if (verbosity >= 3) fprintf(stderr, "kern_pc: %li <--> %li\n", src_leaf_id, node_id);
            result = clEnqueueNDRangeKernel(pts->queue, pts->kern_pc, 1, NULL, &qt->block_size, &qt->block_size, 0, NULL, NULL);
            CHECK_RESULT(result);
        } else {
            // recurse on children
            long children[4] = {
                    qt->nodes[node_id].pp,
                    qt->nodes[node_id].pz,
                    qt->nodes[node_id].zp,
                    qt->nodes[node_id].zz};
            for (int i = 0; i < 4; i++) {
                if (children[i] != 0)
                    ptree_step_node(pts, src_leaf_id, children[i], threshold, verbosity);
            }
        }
    } else {
        long tar_leaf_id = -node_id-1;
        // calculate leaf distance
        float dist = calc_dist_bounds(&qt->leaves.bounds[src_leaf_id], &qt->leaves.bounds[tar_leaf_id]);
        float size = calc_size(qt->leaves.bounds[tar_leaf_id]);
        if (dist / size > threshold) {
            // calculate as single leaf
            arg = (cl_int)(src_leaf_id * qt->block_size);
            result = clSetKernelArg(pts->kern_pl, 4, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            arg = (cl_int)(tar_leaf_id);
            result = clSetKernelArg(pts->kern_pl, 5, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            if (verbosity >= 3) fprintf(stderr, "kern_pl: %li <--> %li\n", src_leaf_id, tar_leaf_id);
            result = clEnqueueNDRangeKernel(pts->queue, pts->kern_pl, 1, NULL, &qt->block_size, &qt->block_size, 0, NULL, NULL);
            CHECK_RESULT(result);
        } else {
            // calculate point-to-point
            arg = (cl_int)(src_leaf_id * qt->block_size);
            result = clSetKernelArg(pts->kern_pp, 5, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            arg = (cl_int)(tar_leaf_id * qt->block_size);
            result = clSetKernelArg(pts->kern_pp, 6, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            arg = (cl_int)(qt->leaves.count[tar_leaf_id]);
            result = clSetKernelArg(pts->kern_pp, 7, sizeof(cl_int), &arg);
            CHECK_RESULT(result);
            if (verbosity >= 3) fprintf(stderr, "kern_pp: %li <--> %li\n", src_leaf_id, tar_leaf_id);
            result = clEnqueueNDRangeKernel(pts->queue, pts->kern_pp, 1, NULL, &qt->block_size, &qt->block_size, 0, NULL, NULL);
            CHECK_RESULT(result);
        }
    }
}

static void ptree_step(void *state, float time_step, float threshold, int verbosity) {
    struct ptree_state *pts = (struct ptree_state *)state;

    // step 1: load data from tree into buffers
    if (verbosity >= 3) fprintf(stderr, "Queueing buffer writes to GPU...\n");
    cl_float2 *zero_buff = calloc(pts->tree.max_leaves * pts->tree.block_size, sizeof(cl_float2));
    cl_int result = clEnqueueWriteBuffer(pts->queue, pts->buff_point_accel, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), zero_buff, 0, NULL, NULL);
    CHECK_RESULT(result);
    free(zero_buff);
    result = clEnqueueWriteBuffer(pts->queue, pts->buff_point_pos, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), pts->tree.points.pos, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueWriteBuffer(pts->queue, pts->buff_point_vel, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), pts->tree.points.vel, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueWriteBuffer(pts->queue, pts->buff_point_mass, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float), pts->tree.points.mass, 0, NULL, NULL);
    CHECK_RESULT(result);

    result = clEnqueueWriteBuffer(pts->queue, pts->buff_leaf_pos, CL_FALSE, 0, pts->tree.max_leaves * sizeof(cl_float2), pts->tree.leaves.pos, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueWriteBuffer(pts->queue, pts->buff_leaf_mass, CL_FALSE, 0, pts->tree.max_leaves * sizeof(cl_float), pts->tree.leaves.mass, 0, NULL, NULL);
    CHECK_RESULT(result);

    result = clEnqueueWriteBuffer(pts->queue, pts->buff_cell_pos, CL_FALSE, 0, pts->tree.max_cells * sizeof(cl_float2), pts->tree.cells.pos, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueWriteBuffer(pts->queue, pts->buff_cell_mass, CL_FALSE, 0, pts->tree.max_cells * sizeof(cl_float), pts->tree.cells.mass, 0, NULL, NULL);
    CHECK_RESULT(result);

    // step 2: iterate tree, performing calculations as needed
    //   note: this iterates over LEAVES, not POINTS
    unsigned int highest_leaf = 0;
    for (unsigned int i = 0; i < pts->tree.max_leaves; i++) {
        if (pts->tree.leaves.count[i] > 0) {
            ptree_step_node(pts, i, pts->tree.root, threshold, verbosity);
            highest_leaf = i;
        }
    }
    cl_float time_step_cl = (cl_float)(time_step);
    result = clSetKernelArg(pts->kern_update, 3, sizeof(cl_float), &time_step_cl);
    CHECK_RESULT(result);
    size_t total_leaf_points = (size_t)(pts->tree.block_size * (highest_leaf + 1));
    if (verbosity >= 3) fprintf(stderr, "kern_update\n");
    result = clEnqueueNDRangeKernel(pts->queue, pts->kern_update, 1, NULL, &total_leaf_points, &pts->tree.block_size, 0, NULL, NULL);
    CHECK_RESULT(result);

    // step 3: read data back to newly allocated host buffers
    cl_float2 *new_pos_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2));
    cl_float2 *new_vel_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2));
    cl_float *new_mass_buff = malloc(pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float));
    size_t *chunk_sizes_buff = malloc(pts->tree.max_leaves * sizeof(size_t));

    // read positions and velocities
    if (verbosity >= 3) fprintf(stderr, "Queueing buffer reads from GPU...\n");
    result = clEnqueueReadBuffer(pts->queue, pts->buff_point_pos, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), new_pos_buff, 0, NULL, NULL);
    CHECK_RESULT(result);
    result = clEnqueueReadBuffer(pts->queue, pts->buff_point_vel, CL_FALSE, 0, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float2), new_vel_buff, 0, NULL, NULL);
    CHECK_RESULT(result);
    memcpy(new_mass_buff, pts->tree.points.mass, pts->tree.max_leaves * pts->tree.block_size * sizeof(cl_float));
    memcpy(chunk_sizes_buff, pts->tree.leaves.count, pts->tree.max_leaves * sizeof(size_t));

    // step 4: use these to "set" the quadtree
    //   note: the masses can be copied from the quadtree, not GPU
    //   note: make sure the GPU finishes its work first.
    if (verbosity >= 3) fprintf(stderr, "Waiting until GPU finishes all assigned work...\n");
    result = clFinish(pts->queue);
    CHECK_RESULT(result);
    if (verbosity >= 3) fprintf(stderr, "Reconstructing tree on host...\n");
    set_quadtree(&pts->tree, new_pos_buff, new_vel_buff, new_mass_buff, pts->tree.block_size, chunk_sizes_buff, pts->tree.max_leaves);
    free(new_pos_buff);
    free(new_vel_buff);
    free(new_mass_buff);
    free(chunk_sizes_buff);
}

static void ptree_step_1(void *state, float step_time, int verbosity) {
    ptree_step(state, step_time, 0.1, verbosity);
}

struct algorithm alg_ptree1 = {
    "ptree",
    ptree_import,
    ptree_export,
    ptree_step_1
};

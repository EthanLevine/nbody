#include <argp.h>
#include <stdlib.h>

#include "alg.h"
#include "gen.h"
#include "opt.h"

const char *argp_program_version = "nbody 0.1";
const char *argp_program_bug_address = "<bitbucket.org/EthanLevine/nbody>";

static char doc[] = "nbody -- a flexible n-body simulator with selectable algorithms";

static struct argp_option options[] = {
    {"algorithm",   'a',    "ALGO",     0,
        "Use ALGO as the simulation algorithm. Must be one of: brute, barneshut."},

    {"input",       'i',    "FILE",     0,
        "Read initial state from FILE. Conflicts with \"generator\" option."},

    {"output",      'o',    "FILE",     0,
        "Write final state to FILE."},

    {"steps",       's',    "STEPS",    0,
        "Perform STEPS iterations. 0 is allowed, and is useful for writing a freshly-generated state."},

    {"gen",         'g',    "GEN",      0,
        "Uses GEN to generate an initial state.  Conflicts with the \"input\" option."},

    {"gen-count",   'c',    "COUNT",    0,
        "Generates COUNT points."},

    {"gen-bounds",  'b',    "SIZE",     0,
        "Generates a state bounded by SIZE in each dimension."},

    {"gen-vel-bounds", 1,   "VEL",      0,
        "Generates a state with velocities bounded by VEL."},

    {"gen-mass-bounds", 2,  "MASS",     0,
        "Generates a state with masses bounded by MASS."},

    {"step-time",   't',    "TIME",     0,
        "Makes each step take TIME seconds."},

    {"verbose",     'v',    0,          0,
        "Prints verbose output. Pass multiple times for increased verbosity."},

    {0}
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;
    int gen_status;

    switch (key) {
        case 'a':
            arguments->algorithm = arg;
            break;
        case 'i':
            arguments->input_file = arg;
            break;
        case 'o':
            arguments->output_file = arg;
            break;
        case 's':
            arguments->steps = atol(arg);
            break;
        case 'g':
            arguments->gen_spec.type = arg;
            break;
        case 'c':
            arguments->gen_spec.count = atol(arg);
            break;
        case 'b':
            arguments->gen_spec.pos_size = atof(arg);
            break;
        case 1:
            arguments->gen_spec.vel_size = atof(arg);
            break;
        case 2:
            arguments->gen_spec.mass_size = atof(arg);
            break;
        case 't':
            arguments->step_time = atof(arg);
            break;
        case 'v':
            arguments->verbosity += 1;
            break;
        
        case ARGP_KEY_END:
            // Check gen_spec.
            gen_status = gen_check_spec(&arguments->gen_spec);
            if (gen_status < 0) {
                argp_error(state, "Invalid combination of generator arguments.");
            }
            // Make sure the algorithm exists, if it was chosen
            if (arguments->algorithm != NULL && get_algorithm(arguments->algorithm) == NULL) {
                argp_error(state, "Algorithm \"%s\" not found.", arguments->algorithm);
            }
            // input_file or gen_spec must be specified
            if (gen_status == 0 && arguments->input_file == NULL) {
                argp_error(state, "Either --generator or --input must be specified.");
            }
            if (gen_status > 0 && arguments->input_file != NULL) {
                argp_error(state, "Only --generator or --input may be specified, not both.");
            }
            // algorithm must be specified if any steps are to be performed
            if (arguments->algorithm == NULL && arguments->steps > 0) {
                argp_error(state, "--algorithm must be specified.");
            }
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, "", doc};

void parse_all(int argc, char **argv, struct arguments *arguments) {
    arguments->algorithm = NULL;
    arguments->input_file = NULL;
    arguments->output_file = NULL;
    arguments->steps = 0;
    arguments->verbosity = 0;
    arguments->step_time = 0;
    arguments->gen_spec.type = NULL;
    arguments->gen_spec.pos_size = 0;
    arguments->gen_spec.vel_size = 0;
    arguments->gen_spec.mass_size = 0;
    arguments->gen_spec.count = 0;

    argp_parse(&argp, argc, argv, 0, 0, arguments);
}
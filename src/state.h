#pragma once

struct point {
    float m;
    float x;
    float y;
    float u;
    float v;
};

struct state {
    struct point *vec;
    long count;
};

void write_state(const char *file, const struct state *state);

void read_state(const char *file, struct state *state);

typedef void *(*import_state_fn)(struct state, int);

typedef struct state (*export_state_fn)(void *, int);

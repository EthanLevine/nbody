#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "alg.h"
#include "state.h"

#include <CL/opencl.h>

#include "alg_pbrute_kern.clpp"

#define OH_NO(res) errx(5, "oh no: %i @ %i", (res), __LINE__)

struct pbrute_state {
    cl_uint count;
    cl_mem pos_buffer;
    cl_mem pos_new_buffer;
    cl_mem vel_buffer;
    cl_mem mass_buffer;

    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;

    size_t block_size;
    size_t buffer_size;
};

static void *pbrute_import(struct state state, int verbosity) {
    verbosity;
    struct pbrute_state *pbs = malloc(sizeof(struct pbrute_state));
    pbs->count = state.count;

    // Fill out the architectural parts of pbrute_state.
    cl_uint num_platforms;
    cl_int result = clGetPlatformIDs(0, NULL, &num_platforms);
    if (result != CL_SUCCESS) OH_NO(result);

    cl_platform_id *platform_ids = malloc(num_platforms * sizeof(cl_platform_id));
    result = clGetPlatformIDs(num_platforms, platform_ids, NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->platform = platform_ids[0];

    cl_uint num_devices;
    result = clGetDeviceIDs(pbs->platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);
    if (result != CL_SUCCESS) OH_NO(result);

    cl_device_id *device_ids = malloc(num_devices * sizeof(cl_device_id));
    result = clGetDeviceIDs(pbs->platform, CL_DEVICE_TYPE_GPU, num_devices, device_ids, NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->device = device_ids[0];

    result = clGetDeviceInfo(pbs->device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(pbs->block_size), &pbs->block_size, NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->context = clCreateContext(NULL, 1, &pbs->device, NULL, NULL, &result);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->queue = clCreateCommandQueue(pbs->context, pbs->device, 0, &result);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->program = clCreateProgramWithSource(pbs->context, 1, &pbrute_kern, NULL, &result);
    if (result != CL_SUCCESS) OH_NO(result);

    result = clBuildProgram(pbs->program, 1, device_ids, "", NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    pbs->kernel = clCreateKernel(pbs->program, "pbrute_kern", &result);
    if (result != CL_SUCCESS) OH_NO(result);

    // Allocate and write buffers.
    pbs->buffer_size = pbs->block_size * ((pbs->count + pbs->block_size - 1) / pbs->block_size);
    //warnx("block: %d,  buffer: %d,  count: %d\n", pbs->block_size, pbs->buffer_size, pbs->count);
    cl_float2 *pos_vec;
    cl_float2 *vel_vec;
    cl_float *mass_vec;
    if (posix_memalign((void**) &pos_vec, sizeof(cl_float2), pbs->buffer_size * sizeof(cl_float2)) != 0)
        err(5, "couldn't allocate position vector");
    if (posix_memalign((void**) &vel_vec, sizeof(cl_float2), pbs->buffer_size * sizeof(cl_float2)) != 0)
        err(5, "couldn't allocate velocity vector");
    if (posix_memalign((void**) &mass_vec, sizeof(cl_float2), pbs->buffer_size * sizeof(cl_float)) != 0)
        err(5, "couldn't allocate mass vector");

    for (unsigned int i = 0; i < pbs->count; i++) {
        cl_float2 pos = {{ state.vec[i].x, state.vec[i].y }};
        cl_float2 vel = {{ state.vec[i].u, state.vec[i].v }};
        pos_vec[i] = pos;
        vel_vec[i] = vel;
        mass_vec[i] = state.vec[i].m;
    }
    for (unsigned int i = pbs->count; i < pbs->buffer_size; i++) {
        cl_float2 pos = {{ 0.0f, 0.0f }};
        cl_float2 vel = {{ 0.0f, 0.0f }};
        pos_vec[i] = pos;
        vel_vec[i] = vel;
        mass_vec[i] = 0.0f;
    }

    pbs->pos_buffer = clCreateBuffer(pbs->context, CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
            pbs->buffer_size * sizeof(cl_float2), pos_vec, &result);
    if (result != CL_SUCCESS) OH_NO(result);
    pbs->vel_buffer = clCreateBuffer(pbs->context, CL_MEM_COPY_HOST_PTR,
            pbs->buffer_size * sizeof(cl_float2), vel_vec, &result);
    if (result != CL_SUCCESS) OH_NO(result);
    pbs->mass_buffer = clCreateBuffer(pbs->context, CL_MEM_COPY_HOST_PTR,
            pbs->buffer_size * sizeof(cl_float), mass_vec, &result);
    if (result != CL_SUCCESS) OH_NO(result);
    pbs->pos_new_buffer = clCreateBuffer(pbs->context, CL_MEM_WRITE_ONLY,
            pbs->buffer_size * sizeof(cl_float2), NULL, &result);
    if (result != CL_SUCCESS) OH_NO(result);
    
    cl_float eps = 0.0001;
    result = clSetKernelArg(pbs->kernel, 1, sizeof(cl_float), &eps);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 2, sizeof(cl_mem), &pbs->pos_buffer);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 3, sizeof(cl_mem), &pbs->vel_buffer);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 4, sizeof(cl_mem), &pbs->mass_buffer);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 5, sizeof(cl_mem), &pbs->pos_new_buffer);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 6, pbs->block_size * sizeof(cl_float2), NULL);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clSetKernelArg(pbs->kernel, 7, pbs->block_size * sizeof(cl_float), NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    free(platform_ids);
    free(device_ids);
    return pbs;
}

static struct state pbrute_export(void *state, int verbosity) {
    verbosity;
    struct pbrute_state *pbs = (struct pbrute_state *)state;
    struct state estate;
    estate.count = pbs->count;
    estate.vec = malloc(pbs->count * sizeof(struct point));

    cl_float *pos_vec;
    cl_float *vel_vec;
    cl_float *mass_vec;
    if (posix_memalign((void**) &pos_vec, sizeof(cl_float2),
            pbs->count * sizeof(cl_float2)) != 0)
        err(5, "couldn't allocate position vector");
    if (posix_memalign((void**) &vel_vec, sizeof(cl_float2),
            pbs->count * sizeof(cl_float2)) != 0)
        err(5, "couldn't allocate velocity vector");
    if (posix_memalign((void**) &mass_vec, sizeof(cl_float2),
            pbs->count * sizeof(cl_float)) != 0)
        err(5, "couldn't allocate mass vector");

    cl_int result = clEnqueueReadBuffer(pbs->queue, pbs->pos_buffer, CL_TRUE, 0, pbs->count * sizeof(cl_float2), pos_vec, 0, NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clEnqueueReadBuffer(pbs->queue, pbs->vel_buffer, CL_TRUE, 0, pbs->count * sizeof(cl_float2), vel_vec, 0, NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clEnqueueReadBuffer(pbs->queue, pbs->mass_buffer, CL_TRUE, 0, pbs->count * sizeof(cl_float), mass_vec, 0, NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);

    for (unsigned int i = 0; i < pbs->count; i++) {
        estate.vec[i].x = pos_vec[2*i];
        estate.vec[i].y = pos_vec[2*i+1];
        estate.vec[i].u = vel_vec[2*i];
        estate.vec[i].v = vel_vec[2*i+1];
        estate.vec[i].m = mass_vec[i];
    }

    free(pos_vec);
    free(vel_vec);
    free(mass_vec);

    return estate;
}

static void pbrute_step(void *state, float step_time, int verbosity) {
    verbosity;
    struct pbrute_state *pbs = (struct pbrute_state *)state;
    cl_float step_time_f = (cl_float)step_time;
    cl_int result = clSetKernelArg(pbs->kernel, 0, sizeof(cl_float), &step_time_f);
    if (result != CL_SUCCESS) OH_NO(result);

    // perform the computation
    result = clEnqueueNDRangeKernel(pbs->queue, pbs->kernel, 1, NULL, &pbs->buffer_size, &pbs->block_size, 0, NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);
    // copy new positions to pos_buffer.
    result = clEnqueueCopyBuffer(pbs->queue, pbs->pos_new_buffer, pbs->pos_buffer, 0, 0, pbs->buffer_size * sizeof(cl_float2), 0, NULL, NULL);
    if (result != CL_SUCCESS) OH_NO(result);
    result = clFinish(pbs->queue);
    if (result != CL_SUCCESS) OH_NO(result);
}

struct algorithm alg_pbrute = {
    "pbrute",
    pbrute_import,
    pbrute_export,
    pbrute_step
};
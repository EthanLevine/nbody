__kernel void pbrute_kern(float step_time,
                          float eps,
                          __global float2 *pos,
                          __global float2 *vel,
                          __global float *mass,
                          __global float2 *pos_new,
                          __local float2 *pblock,
                          __local float *mblock) {

    float2 dt = (float2)(step_time, step_time);

    int global_id = get_global_id(0);
    int local_id = get_local_id(0);

    int point_count = get_global_size(0);
    int block_size = get_local_size(0);
    int block_count = point_count/block_size;

    float2 local_pos = pos[global_id];
    float2 local_accel = (float2)(0, 0);

    for (int i = 0; i < block_count; i++) {
        pblock[local_id] = pos[i*block_size + local_id];
        mblock[local_id] = mass[i*block_size + local_id];
        barrier(CLK_LOCAL_MEM_FENCE);
        for (int j = 0; j < block_size; j++) {
            float2 remote_pos = pblock[j];
            float2 dist = remote_pos - local_pos;
            float R_inv = rsqrt(dist.x*dist.x + dist.y*dist.y + eps);
            local_accel += dist * (6.673e-11) * R_inv * mblock[j] * R_inv * R_inv;
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    pos_new[global_id] = local_pos + dt*vel[global_id];
    vel[global_id] += dt*local_accel;
}

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "state.h"

#define FIELD_SPEC "%.20e"
#define LINE_SPEC FIELD_SPEC "\t" FIELD_SPEC "\t" FIELD_SPEC "\t" FIELD_SPEC "\t" FIELD_SPEC "\n"
#define READ_FIELD_SPEC "%e"
#define READ_LINE_SPEC READ_FIELD_SPEC "\t" READ_FIELD_SPEC "\t" READ_FIELD_SPEC "\t" READ_FIELD_SPEC "\t" READ_FIELD_SPEC "\n"

#define RESID_SPEC FIELD_SPEC "\t" FIELD_SPEC "\t" FIELD_SPEC "\t" FIELD_SPEC "\n"

void write_state(const char *file, const struct state *state) {
    // open the file
    FILE *out = fopen(file, "w");
    if (out == NULL) {
        err(1, "Output file \"%s\" could not be opened.", file);
    }

    // for all points
    for (int pt_id = 0; pt_id < state->count; pt_id++) {
        // write this point
        struct point *pt = &state->vec[pt_id];
        if (fprintf(out, LINE_SPEC,
                pt->m, pt->x, pt->y, pt->u, pt->v) < 0) {
            int temperr = errno;
            fclose(out);
            errno = temperr;
            err(1, "Error writing to output file.");
        }
    }

    if (fclose(out) != 0) {
        err(1, "Error closing output file.");
    }
}

void read_state(const char *file, struct state *state) {
    int cur_size = 1;
    int cur_id = 0;
    state->vec = malloc(cur_size * sizeof(struct point));

    FILE *in = fopen(file, "r");
    if (in == NULL) {
        err(1, "Input file \"%s\" could not be opened.", file);
    }

    while (1) {
        if (cur_id + 1> cur_size) {
            cur_size *= 2;
            state->vec = realloc(state->vec, cur_size * sizeof(struct point));
        }
        struct point *pt = &state->vec[cur_id];
        int retval = fscanf(in, READ_LINE_SPEC, &pt->m, &pt->x, &pt->y, &pt->u, &pt->v);
        if (retval == EOF || retval < 5) {
            break;
        }
        cur_id++;
    }
    state->count = cur_id;
    state->vec = realloc(state->vec, state->count * sizeof(struct point));
    if (fclose(in) != 0) {
        err(1, "Error reading input file.");
    }
}

void write_residual(const char *file, const struct state *state1, const struct state *state2) {
    if (state1->count != state2->count) {
        errx(1, "States have different point counts: %li and %li.", state1->count, state2->count);
    }
    FILE *out = fopen(file, "w");
    if (out == NULL) {
        err(1, "Residual file \"%s\" could not be opened.", file);
    }

    for (int pt_id = 0; pt_id < state1->count; pt_id++) {
        struct point *pt1 = &state1->vec[pt_id];
        struct point *pt2 = &state2->vec[pt_id];
        if (fprintf(out, RESID_SPEC,
                pt1->x - pt2->x, pt1->y - pt2->y, pt1->u - pt2->u, pt1->v - pt2->v) < 0) {
            int temperr = errno;
            fclose(out);
            errno = temperr;
            err(1, "Error writing to residual file.");
        }
    }

    if (fclose(out) != 0) {
        err(1, "Error closing output file.");
    }
}

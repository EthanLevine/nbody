#pragma once

#include "state.h"

#define pdist(a, b) sqrtf(((a).x-(b).x)*((a).x-(b).x) + ((a).y-(b).y)*((a).y-(b).y))

typedef void (*alg_step_fn)(void *, float, int);

struct algorithm {
    char *name;
    import_state_fn import;
    export_state_fn export;
    alg_step_fn step;
};

struct algorithm alg_brute;
struct algorithm alg_bh1;
struct algorithm alg_pbrute;
struct algorithm alg_ptree1;

void add_algorithms();

struct algorithm *get_algorithm(char *name);


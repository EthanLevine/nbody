#include <float.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_math.h>

#include "alg.h"
#include "state.h"

// Note:  Node indices have multiple meanings:
// * 0:  empty
// * positive:  quadnode index
// * negative:  negative (-1) point index
struct quadnode {
    long pp;
    long pz;
    long zp;
    long zz;
    float x;
    float y;
    float m;
};

struct quadtree {
    struct quadnode *nodes;
    long node_count;
    long node_slots;
    float xmin;
    float xmax;
    float ymin;
    float ymax;
};

struct bh_state {
    struct state inner;
};

static long *quadtree_find_child(struct quadnode *node, struct point *pt,
        float *xmin, float *xmax, float *ymin, float *ymax) {
    if (pt->x > (*xmax + *xmin)/2) {
        if (pt->y > (*ymax + *ymin)/2) {
            *xmin = (*xmax + *xmin)/2;
            *ymin = (*ymax + *ymin)/2;
            return &node->pp;
        } else {
            *xmin = (*xmax + *xmin)/2;
            *ymax = (*ymax + *ymin)/2;
            return &node->pz;
        }
    } else {
        if (pt->y > (*ymax + *ymin)/2) {
            *xmax = (*xmax + *xmin)/2;
            *ymin = (*ymax + *ymin)/2;
            return &node->zp;
        } else {
            *xmax = (*xmax + *xmin)/2;
            *ymax = (*ymax + *ymin)/2;
            return &node->zz;
        }
    }
}

static void clear_quadnode(struct quadnode *node) {
    node->pp = 0;
    node->pz = 0;
    node->zp = 0;
    node->zz = 0;
    node->x = 0;
    node->y = 0;
    node->m = 0;
}

#define UPDATE_QUADNODE_ADD(node, add) {\
    (node)->x = ((node)->x * (node)->m + (add)->x * (add)->m) / ((node)->m + (add)->m);\
    (node)->y = ((node)->y * (node)->m + (add)->y * (add)->m) / ((node)->m + (add)->m);\
    (node)->m += (add)->m;\
}

#define UPDATE_QUADNODE_REMOVE(node, rem) {\
    (node)->x = ((node)->x * (node)->m - (rem)->x * (rem)->m) / ((node)->m - (rem)->m);\
    (node)->y = ((node)->y * (node)->m - (rem)->y * (rem)->m) / ((node)->m - (rem)->m);\
    (node)->m -= (rem)->m;\
}

static void quadtree_insert(struct quadtree *quadtree, struct bh_state *state, long node_id,
        long pt_id, float xmin, float xmax, float ymin, float ymax) {
    long *child = quadtree_find_child(&quadtree->nodes[node_id],
            &state->inner.vec[pt_id], &xmin, &xmax, &ymin, &ymax);
    if (*child == 0) {
        // empty node - put point there.
        *child = -pt_id-1;
    } else if (*child > 0) {
        // quadnode here - follow.
        quadtree_insert(quadtree, state, *child, pt_id, xmin, xmax, ymin, ymax);
    } else {
        // point here - make new quadnode and insert twice
        // expand if needed
        if (quadtree->node_slots == quadtree->node_count) {
            ptrdiff_t child_offset = (char*)child - (char*)quadtree->nodes;
            quadtree->node_slots *= 2;
            quadtree->nodes = realloc(quadtree->nodes, quadtree->node_slots * sizeof(struct quadnode));
            child = (long*)((char*)quadtree->nodes + child_offset);
        }
        clear_quadnode(&quadtree->nodes[quadtree->node_count]);
        quadtree_insert(quadtree, state, quadtree->node_count, -(*child)-1, xmin,
                xmax, ymin, ymax);
        *child = quadtree->node_count;
        quadtree->node_count++;
        quadtree_insert(quadtree, state, *child, pt_id, xmin, xmax, ymin, ymax);
    }
    // update the quadnode's x, y, m.
    UPDATE_QUADNODE_ADD(&quadtree->nodes[node_id], &state->inner.vec[pt_id]);
}

static void dump_qt(struct quadtree *qt, int node_id, int level) {
    if (node_id == 0 && level > 0) return;
    printf("%*s%d\n", level, "", node_id);
    if (node_id < 0) return;
    
    struct quadnode *node = &qt->nodes[node_id];
    level += 2;
    dump_qt(qt, node->pp, level);
    dump_qt(qt, node->pz, level);
    dump_qt(qt, node->zp, level);
    dump_qt(qt, node->zz, level);
}

static struct quadtree *build_quadtree(struct bh_state *state) {
    struct quadtree *quadtree = malloc(sizeof(struct quadtree));
    quadtree->nodes = malloc(sizeof(struct quadnode));
    quadtree->node_count = 1;
    quadtree->node_slots = 1;
    clear_quadnode(&quadtree->nodes[0]);

    // first, find bounds of the system
    quadtree->xmin = INFINITY;
    quadtree->xmax = -INFINITY;
    quadtree->ymin = INFINITY;
    quadtree->ymax = -INFINITY;
    for (int i = 0; i < state->inner.count; i++) {
        struct point *pt = &state->inner.vec[i];
        if (pt->x < quadtree->xmin) quadtree->xmin = pt->x;
        if (pt->x > quadtree->xmax) quadtree->xmax = pt->x;
        if (pt->y < quadtree->ymin) quadtree->ymin = pt->y;
        if (pt->y > quadtree->ymax) quadtree->ymax = pt->y;
    }
    // round up to square
    if (quadtree->xmax - quadtree->xmin > quadtree->ymax - quadtree->ymin) {
        float yadj = ((quadtree->xmax - quadtree->xmin) - (quadtree->ymax - quadtree->ymin)) / 2;
        quadtree->ymax += yadj;
        quadtree->ymin -= yadj;
    } else {
        float xadj = ((quadtree->ymax - quadtree->ymin) - (quadtree->xmax - quadtree->xmin)) / 2;
        quadtree->xmax += xadj;
        quadtree->xmin -= xadj;
    }
    // Now add points to the tree
    for (int i = 0; i < state->inner.count; i++) {
        quadtree_insert(quadtree, state, 0, i, quadtree->xmin, quadtree->xmax,
                quadtree->ymin, quadtree->ymax);
    }
    //dump_qt(quadtree, 0, 0);
    return quadtree;
}

static void *bh_import(struct state state, int verbosity) {
    verbosity;
    struct bh_state *bs = malloc(sizeof(*bs));
    bs->inner.vec = malloc(state.count * sizeof(struct point));
    bs->inner.count = state.count;
    memcpy(bs->inner.vec, state.vec, state.count * sizeof(struct point));
    return bs;
}

static struct state bh_export(void *state, int verbosity) {
    verbosity;
    return ((struct bh_state *)state)->inner;
}

static void factor_quadnode(struct quadtree *quadtree, struct bh_state *state,
        long node_id, long pt_id, float threshold,
        float *accel_x, float *accel_y, float dim) {
    // so many parameters!
    // if node_id == 0, do nothing (empty cell)
    if (node_id > 0 || dim * 1.5 > (quadtree->xmax - quadtree->xmin)) {
        float R = gsl_hypot(quadtree->nodes[node_id].x - state->inner.vec[pt_id].x,
                quadtree->nodes[node_id].y - state->inner.vec[pt_id].y);
        if (dim / R < threshold) {
            float force_comp = GSL_CONST_MKSA_GRAVITATIONAL_CONSTANT *
                    quadtree->nodes[node_id].m / (R*R*R);
            *accel_x += (quadtree->nodes[node_id].x - state->inner.vec[pt_id].x)
                    * force_comp;
            *accel_y += (quadtree->nodes[node_id].y - state->inner.vec[pt_id].y)
                    * force_comp;
        } else {
            // Recurse on children!
            factor_quadnode(quadtree, state, quadtree->nodes[node_id].pp, pt_id,
                    threshold, accel_x, accel_y, dim/2);
            factor_quadnode(quadtree, state, quadtree->nodes[node_id].pz, pt_id,
                    threshold, accel_x, accel_y, dim/2);
            factor_quadnode(quadtree, state, quadtree->nodes[node_id].zp, pt_id,
                    threshold, accel_x, accel_y, dim/2);
            factor_quadnode(quadtree, state, quadtree->nodes[node_id].zz, pt_id,
                    threshold, accel_x, accel_y, dim/2);
        }
    } else if (node_id < 0) {
        long other_pt_id = -node_id-1;
        if (other_pt_id != pt_id) {
            float R = gsl_hypot(state->inner.vec[other_pt_id].x - state->inner.vec[pt_id].x,
                    state->inner.vec[other_pt_id].y - state->inner.vec[pt_id].y);
            float force_comp = GSL_CONST_MKSA_GRAVITATIONAL_CONSTANT *
                    state->inner.vec[other_pt_id].m / (R*R*R);
            *accel_x += (state->inner.vec[other_pt_id].x - state->inner.vec[pt_id].x) * force_comp;
            *accel_y += (state->inner.vec[other_pt_id].y - state->inner.vec[pt_id].y) * force_comp;
        }
    }
}

static void bh_step(void *state, float step_time, float threshold,
        int verbosity) {
    verbosity;
    struct bh_state *bhs = (struct bh_state *)state;
    struct quadtree *qt = build_quadtree(bhs);
    // update all positions
    float *pos_x_new = malloc(bhs->inner.count * sizeof(float));
    float *pos_y_new = malloc(bhs->inner.count * sizeof(float));
    for (int i = 0; i < bhs->inner.count; i++) {
        pos_x_new[i] = bhs->inner.vec[i].x + step_time * bhs->inner.vec[i].u;
        pos_y_new[i] = bhs->inner.vec[i].y + step_time * bhs->inner.vec[i].v;
    }
    // update velocities using BH algorithm
    for (int i = 0; i < bhs->inner.count; i++) {
        float accel_x = 0;
        float accel_y = 0;
        factor_quadnode(qt, bhs, 0, i, threshold, &accel_x, &accel_y,
                qt->xmax - qt->xmin);
        bhs->inner.vec[i].u += step_time * accel_x;
        bhs->inner.vec[i].v += step_time * accel_y;
    }
    // copy new positions back
    for (int i = 0; i < bhs->inner.count; i++) {
        bhs->inner.vec[i].x = pos_x_new[i];
        bhs->inner.vec[i].y = pos_y_new[i];
    }
    free(pos_x_new);
    free(pos_y_new);

    free(qt->nodes);
    free(qt);
}

static void bh_step_1(void *state, float step_time, int verbosity) {
    bh_step(state, step_time, 0.1, verbosity);
}

struct algorithm alg_bh1 = {
    "bh",
    bh_import,
    bh_export,
    bh_step_1
};

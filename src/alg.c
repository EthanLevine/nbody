#include <stdlib.h>
#include <string.h>

#include "alg.h"

#define MAX_ALGORITHMS 100

static struct algorithm algs[MAX_ALGORITHMS];
static int alg_count = 0;

static void add_algorithm(struct algorithm *alg) {
    if (alg_count == MAX_ALGORITHMS) {
        return; // can't print - should probably error out.
    }
    algs[alg_count] = *alg;
    alg_count++;
}

void add_algorithms() {
    add_algorithm(&alg_brute);
    add_algorithm(&alg_bh1);
    add_algorithm(&alg_pbrute);
    add_algorithm(&alg_ptree1);
}

struct algorithm *get_algorithm(char *name) {
    for (int i = 0; i < alg_count; i++) {
        if (strcmp(algs[i].name, name) == 0) {
            return &algs[i];
        }
    }
    return NULL;
}

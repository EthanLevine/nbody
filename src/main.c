#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "alg.h"
#include "opt.h"
#include "state.h"

static double get_time() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + (tv.tv_usec * 1.0E-6);
}

int main(int argc, char **argv) {
    struct arguments arguments;
    struct algorithm *algorithm;
    struct state flat_state;
    void *alg_state;

    // Add any algorithms here
    add_algorithms();

    // Parse command-line options
    parse_all(argc, argv, &arguments);

    double span_min_step = INFINITY;
    double span_max_step = 0;
    double span_avg_step = 0;
    double span_import = 0;
    double span_export = 0;

    // Get the state (from reading or from generation)
    if (arguments.input_file != NULL) {
        read_state(arguments.input_file, &flat_state);
    }
    if (arguments.gen_spec.type != NULL) {
        // generate initial state
        gen_state(&arguments.gen_spec, &flat_state, arguments.verbosity);
    }

    // Load algorithm and state, if needed.
    if (arguments.algorithm != NULL) {
        algorithm = get_algorithm(arguments.algorithm);
        double time_import_start = get_time();
        alg_state = algorithm->import(flat_state, arguments.verbosity);
        span_import = get_time() - time_import_start;
        // Note:  This means the import functions should never copy the state blindly.
        free(flat_state.vec);

        // Run any required steps
        for (int i = 0; i < arguments.steps; i++) {
            double time_step_start = get_time();
            algorithm->step(alg_state, arguments.step_time, arguments.verbosity);
            double span_this_step = get_time() - time_step_start;
            if (span_min_step == -1 || span_min_step > span_this_step)
                span_min_step = span_this_step;
            if (span_max_step < span_this_step)
                span_max_step = span_this_step;
            span_avg_step = (span_avg_step * i + span_this_step) / (i+1);
        }
    }

    // Save the state
    if (arguments.output_file != NULL) {
        if (arguments.algorithm != NULL) {
            // Note:  This means the export functions should free their states if necessary.
            double time_export_start = get_time();
            flat_state = algorithm->export(alg_state, arguments.verbosity);
            span_export = get_time() - time_export_start;
        }
        write_state(arguments.output_file, &flat_state);
    }

    if (arguments.verbosity >= 1) {
        // print out the timing stats
        fprintf(stderr, "Algorithm:  %s     Particles:  %li\n", arguments.algorithm, flat_state.count);
        fprintf(stderr, "Overhead:   %10.4f    (%10.4f    +  %10.4f)\n", span_import + span_export, span_import, span_export);
        fprintf(stderr, "Step time:  %10.4f    (%10.4f    -> %10.4f)\n\n", span_avg_step, span_min_step, span_max_step);
    }

    return 0;
}
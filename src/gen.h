#pragma once

#include "state.h"

struct gen_spec {
    char *type;
    float pos_size;
    float vel_size;
    float mass_size;
    long count;
};

int gen_check_spec(struct gen_spec *spec);

void gen_state(struct gen_spec *spec, struct state *state, int verbosity);
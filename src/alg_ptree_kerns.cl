__kernel void ptree_kern_pp(__global float2 *ppos,
                            __global float *pmass,
                            __global float2 *paccel,
                            __local float2 *lppos,
                            __local float *lpmass,
                            int src_leaf_offset,
                            int target_leaf_offset,
                            int target_leaf_size) {
    int local_id = get_local_id(0);

    // copy source data to private mem
    float2 source_pos = ppos[src_leaf_offset + local_id];
    float2 accel_adj = 0;

    // copy target data to local mem
    lppos[local_id] = ppos[target_leaf_offset + local_id];
    lpmass[local_id] = pmass[target_leaf_offset + local_id];
    barrier(CLK_LOCAL_MEM_FENCE);

    // loop through target_leaf_size points in local mem
    for (int i = 0; i < target_leaf_size; i++) {
        float2 target_pos = lppos[i];
        float2 dist = target_pos - source_pos;
        float R_inv = rsqrt(dist.x*dist.x + dist.y*dist.y + 0.0001);
        accel_adj += dist * (6.673e-11) * R_inv * lpmass[i] * R_inv * R_inv;
    }

    // update accel
    paccel[src_leaf_offset + local_id] += accel_adj;
}

__kernel void ptree_kern_pc(__global float2 *ppos,
                            __global float2 *paccel,
                            __constant float2 *cpos,
                            __constant float *cmass,
                            int src_leaf_offset,
                            int target_cell_id) {
    int local_id = get_local_id(0);

    // get the distance
    float2 dist = cpos[target_cell_id] - ppos[src_leaf_offset + local_id];

    // update the accel
    float R_inv = rsqrt(dist.x*dist.x + dist.y*dist.y);
    paccel[src_leaf_offset + local_id] += dist * (6.673e-11) * R_inv * cmass[target_cell_id] * R_inv * R_inv;
}

__kernel void ptree_kern_pl(__global float2 *ppos,
                            __global float2 *paccel,
                            __constant float2 *lpos,
                            __constant float *lmass,
                            int src_leaf_offset,
                            int target_leaf_id) {
    int local_id = get_local_id(0);

    // get the distance
    float2 dist = lpos[target_leaf_id] - ppos[src_leaf_offset + local_id];

    // update the accel
    float R_inv = rsqrt(dist.x*dist.x + dist.y*dist.y);
    paccel[src_leaf_offset + local_id] += dist * (6.673e-11) * R_inv * lmass[target_leaf_id] * R_inv * R_inv;
}

__kernel void ptree_kern_update(__global float2 *ppos,
                                __global float2 *pvel,
                                __global float2 *paccel,
                                float step_time) {
    float2 dt = (float2)(step_time, step_time);

    int point_id = get_global_id(0) + get_local_id(0);

    ppos[point_id] += dt * pvel[point_id];
    pvel[point_id] += dt * paccel[point_id];
}
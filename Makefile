MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-builtin-variables
MAKEFLAGS += --no-print-directory

BINDIR = bin
OBJDIR = obj
SRCDIR = src

DEFINES = _GNU_SOURCE
WARNERRORS = implicit-function-declaration return-type
WARNIGNORE = unused-value missing-field-initializers
INCLUDES = 

CC = gcc
CFLAGS = -c -Wall -Wextra -std=gnu99 -O2 -g -pedantic \
	$(foreach werr,$(WARNERRORS),-Werror=$(werr)) \
	$(foreach wig,$(WARNIGNORE),-Wno-$(wig)) \
	$(foreach def,$(DEFINES),-D$(def)) \
	$(foreach inc,$(INCLUDES),-I$(inc))
LD = gcc
LDFLAGS = `gsl-config --libs` -lOpenCL

SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SOURCES))

build : bin/nbody

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(SRCDIR)/alg_pbrute_kern.clpp $(SRCDIR)/alg_ptree_kerns.clpp
	$(CC) $(CFLAGS) $< -o $@

$(SRCDIR)/%.clpp : $(SRCDIR)/%.cl
	sed '/^$$/d;s/\\/\\\\/g;s/"/\\"/g;s/^\(.*\)$$/"\1\\n"/;s/^"__kernel void \([^(]*\)/static const char *\1 = "__kernel void \1/;s/^"}\\n"/"}"\;/' $< > $@

bin/nbody : $(OBJECTS)
	$(LD) $(LDFLAGS) $^ -o $@

clean :
	rm -f $(OBJDIR)/*
	rm -f $(BINDIR)/*
	rm -f $(SRCDIR)/*.clpp

rebuild :
	make clean
	make build


#!/usr/bin/env python

from subprocess import call, STDOUT
from sys import argv, exit
from time import time

particle_counts = [1000, 2000, 3000, 4000, 5000, 7500, 10000, 20000, 30000, 40000, 50000, 75000, 100000];

size_bound = 1E3 #m
total_mass = 1.5E10 #kg
vel_bound = 1E-1 #m/s
time_step = 1E2 #s
algs = ["brute", "bh", "pbrute", "ptree"]
step_count = 10

# Create the base state
for particle_count in particle_counts:

    mass_bound = 4 * total_mass / (3 * particle_count)

    file_prefix = "count_%i" % particle_count

    call(["../bin/nbody",
            "-g", "plain",
            "-c", str(particle_count),
            "--gen-bounds", str(size_bound),
            "--gen-vel-bounds", str(vel_bound),
            "--gen-mass-bounds", str(4 * total_mass / (3 * particle_count)),
            "-o", file_prefix + "_base"])

    for alg in algs:
        call(["../bin/nbody",
                "-i", file_prefix + "_base",
                "-a", alg,
                "-t", str(time_step),
                "-s", str(step_count),
                "-o", file_prefix + "_%s_%i" % (alg, step_count),
                "-v"])
